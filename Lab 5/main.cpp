// Based loosly on the first triangle OpenGL tutorial
// http://www.opengl.org/wiki/Tutorial:_OpenGL_3.1_The_First_Triangle_%28C%2B%2B/Win%29
// This program will render two triangles
// Most of the OpenGL code for dealing with buffer objects, etc has been moved to a 
// utility library, to make creation and display of mesh objects as simple as possible

// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtc/type_ptr.hpp> 
#include <stack>

using namespace std;

// Globals
// Real programs don't use globals :-D
// Data would normally be read from files
	
GLfloat colours[] = {	1.0f, 0.0f, 0.0f, 
						0.0f, 1.0f, 0.0f, 
						0.0f, 0.0f, 1.0f, 
						0.0f, 0.0f, 0.0f };

GLuint meshObjects[1];

GLfloat dx, dy = 0.0f;
GLfloat rotateAngle = 0.0f;
GLfloat size = 1.0f;

GLuint shaderProgram;
GLuint shaderProgram1; // phong
GLuint shaderProgram2; // gouraud

GLuint cubeVertCount = 8;

GLuint cubeIndexCount = 36;

GLuint cubeIndices[] = {	0,1,2, 0,2,3, // back 
							1,0,5, 0,4,5, // left 
							6,3,2, 3,6,7, // right 
							1,5,6, 1,6,2, // top 
							0,3,4, 3,7,4, // bottom 
							6,5,4, 7,6,4 }; // front

GLfloat cubeVerts[] = { -0.5, -0.5f, -0.5f, 
						-0.5, 0.5f, -0.5f, 
						0.5, 0.5f, -0.5f, 
						0.5, -0.5f, -0.5f, 
						-0.5, -0.5f, 0.5f, 
						-0.5, 0.5f, 0.5f, 
						0.5, 0.5f, 0.5f, 
						0.5, -0.5f, 0.5f };

GLfloat cubeColours[] = {	0.0f, 0.0f, 0.0f, 
							0.0f, 1.0f, 0.0f, 
							1.0f, 1.0f, 0.0f, 
							1.0f, 0.0f, 0.0f, 
							0.0f, 0.0f, 1.0f, 
							0.0f, 1.0f, 1.0f,
							1.0f, 1.0f, 1.0f, 
							1.0f, 0.0f, 1.0f };

std::stack<glm::mat4> mvStack; 

GLfloat lightR, lightG, lightB = 0.2f;

GLfloat lightDX, lightDY = 0.0f;
GLfloat lightDZ = 0.01f;



// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void init(void) {
	glEnable(GL_DEPTH_TEST); // enable depth testing
	// For this simple example we'll be using the most basic of shader programs
	shaderProgram1 = rt3d::initShaders("gouraud.vert", "simple.frag"); 
	shaderProgram2 = rt3d::initShaders("phong.vert", "phong.frag");
	// Going to create our mesh objects here
	meshObjects[0] = rt3d::createMesh(cubeVertCount, cubeVerts, colours, cubeVerts, cubeVerts, cubeIndexCount, cubeIndices); 
}

void draw(SDL_Window * window) {
	// clear the screen
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	rt3d::lightStruct light0 = {
			{lightR, lightG, lightB, 1.0f},// ambient
			{0.7f, 0.7f, 0.7f, 1.0f},// diffuse
			{0.8f, 0.8f, 0.8f, 1.0f},// specular
			{lightDX, lightDY, lightDZ, 1.0f}// position
	};

	rt3d::materialStruct material0 = {
			{0.4f, 0.2f, 0.2f, 1.0f},// ambient
			{0.8f, 0.5f, 0.5f, 1.0f},// diffuse
			{1.0f, 0.8f, 0.8f, 1.0f},// specular
			2.0f// shininess
	};

	rt3d::materialStruct material1 = {
			{0.4f, 0.2f, 0.8f, 1.0f},// ambient
			{0.3f, 0.1f, 0.5f, 1.0f},// diffuse
			{1.0f, 0.4f, 0.8f, 1.0f},// specular
			2.0f// shininess
	};

	rt3d::materialStruct material2 = {
			{0.8f, 0.1f, 0.4f, 1.0f},// ambient
			{0.6f, 0.4f, 0.5f, 1.0f},// diffuse
			{0.2f, 0.2f, 0.2f, 1.0f},// specular
			1.0f// shininess
	};

	rt3d::materialStruct material3 = {
			{2.4f, 0.5f, 0.8f, 1.0f},// ambient
			{2.8f, 0.5f, 0.5f, 1.0f},// diffuse
			{1.0f, 3.8f, 0.8f, 1.0f},// specular
			2.0f// shininess
	};
	
	rt3d::materialStruct material4 = {
			{0.4f, 0.2f, 0.2f, 1.0f},// ambient
			{4.8f, 2.4f, 2.5f, 1.0f},// diffuse
			{1.0f, 0.8f, 0.8f, 1.0f},// specular
			4.5f// shininess
	};

	rt3d::materialStruct material5 = {
			{0.2f, 0.1f, 2.2f, 1.0f},// ambient
			{0.1f, 0.3f, 0.2f, 1.0f},// diffuse
			{1.0f, 2.8f, 0.8f, 1.0f},// specular
			1.0f// shininess
	};

	// set up projection
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f, 800.0f/600.0f, 1.0f, 50.0f);
	rt3d::setUniformMatrix4fv(shaderProgram1, "projection", glm::value_ptr(projection));

	glUseProgram(shaderProgram1);
	rt3d::setLight(shaderProgram1, light0);
	rt3d::setMaterial(shaderProgram1, material0);

	

	// centre top cube
	glm::mat4 modelview(1.0);
	mvStack.push(modelview); // push modelview to stack
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, 1.0f, -5.0f));
	mvStack.top() = glm::rotate(mvStack.top(), rotateAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram1, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES);

	// cube top right
	mvStack.push(mvStack.top()); // push modelview to stack
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(2.0f, 0.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), rotateAngle/rotateAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setMaterial(shaderProgram1, material1);
	rt3d::setUniformMatrix4fv(shaderProgram1, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES);

	mvStack.pop();
	
	// cube top left
	mvStack.push(mvStack.top()); // push modelview to stack
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-2.0f, 0.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), rotateAngle/rotateAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setMaterial(shaderProgram1, material2);
	rt3d::setUniformMatrix4fv(shaderProgram1, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES);

	mvStack.pop();

	// cube bottom left
	mvStack.push(mvStack.top()); // push modelview to stack
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-2.0f, -2.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), rotateAngle/rotateAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setMaterial(shaderProgram1, material3);
	rt3d::setUniformMatrix4fv(shaderProgram1, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES);

	mvStack.pop();

	// cube bottom centre
	mvStack.push(mvStack.top()); // push modelview to stack
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(0.0f, -2.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), rotateAngle/rotateAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setMaterial(shaderProgram1, material4);
	rt3d::setUniformMatrix4fv(shaderProgram1, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES);

	mvStack.pop();

	// cube bottom right
	mvStack.push(mvStack.top()); // push modelview to stack
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(2.0f, -2.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), rotateAngle/rotateAngle, glm::vec3(0.0f, 1.0f, 0.0f));
	rt3d::setMaterial(shaderProgram1, material5);
	rt3d::setUniformMatrix4fv(shaderProgram1, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshObjects[0],cubeIndexCount,GL_TRIANGLES);

	mvStack.pop();
	
    SDL_GL_SwapWindow(window); // swap buffers
}

void update() {
	Uint8 *keys = SDL_GetKeyboardState(NULL); 
	
	if ( keys[SDL_SCANCODE_W] ) lightDY += 0.1f;
	if ( keys[SDL_SCANCODE_S] ) lightDY -= 0.1f; 
	if ( keys[SDL_SCANCODE_A] ) lightDX -= 0.1f;
	if ( keys[SDL_SCANCODE_D] ) lightDX += 0.1f;
	if ( keys[SDL_SCANCODE_Q] ) lightDZ += 0.1f;
	if ( keys[SDL_SCANCODE_E] ) lightDZ -= 0.1f;

	if ( keys[SDL_SCANCODE_COMMA] ) rotateAngle += 0.5f;
	if ( keys[SDL_SCANCODE_PERIOD] ) rotateAngle -= 0.5f;
	
	if ( keys[SDL_SCANCODE_R] ) lightR += 0.1f;
	if ( keys[SDL_SCANCODE_G] ) lightG += 0.1f;
	if ( keys[SDL_SCANCODE_B] ) lightB += 0.1f;

	if ( lightR > 4 )
		lightR = 0.2f;
	if ( lightG > 4 )
		lightG = 0.2f;
	if ( lightB > 4 ) 
		lightB = 0.2f;
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		update();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}